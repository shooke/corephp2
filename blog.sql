-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2015-05-19 10:32:27
-- 服务器版本： 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- 表的结构 `cp_meeting`
--

CREATE TABLE IF NOT EXISTS `cp_meeting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company` varchar(250) DEFAULT NULL COMMENT '公司名称',
  `name` varchar(10) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  `remarks` text COMMENT '备注',
  `dateline` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=39 ;

--
-- 转存表中的数据 `cp_meeting`
--

INSERT INTO `cp_meeting` (`id`, `company`, `name`, `mobile`, `remarks`, `dateline`) VALUES
(37, 'sdf', 'dfs', '15963100710', '', 1432018797),
(38, 'sdf', 'dfs', '15963100712', 'dfd', 1432018856);

-- --------------------------------------------------------

--
-- 表的结构 `cp_t`
--

CREATE TABLE IF NOT EXISTS `cp_t` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  `mobile` varchar(11) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- 转存表中的数据 `cp_t`
--

INSERT INTO `cp_t` (`id`, `name`, `mobile`) VALUES
(1, 'n1', 'm1'),
(2, 'n2', 'm2'),
(3, 'n3', 'm3'),
(4, 'n1', 'm1'),
(5, 'n2', 'm2'),
(6, 'n3', 'm3'),
(7, 'n1', 'm1'),
(8, 'n1', 'm1'),
(9, 'n1', 'm1'),
(10, 'n1', 'm1'),
(11, 'n1', 'm1'),
(12, 'n1', 'm1'),
(13, 'n1', 'm1'),
(14, 'n1', 'm1'),
(15, 'n1', 'm1'),
(16, 'n1', 'm1'),
(17, 'n1', 'm1'),
(18, 'n1', 'm1'),
(19, 'n1', 'm1'),
(20, 'n1', 'm1'),
(21, 'n1', 'm1'),
(22, 'n1', 'm1'),
(23, 'n1', 'm1'),
(24, 'n1', 'm1'),
(25, 'n1', 'm1'),
(26, 'n1', 'm1'),
(27, 'n1', 'm1'),
(28, 'n1', 'm1'),
(29, 'n1', 'm1'),
(30, 'n1', 'm1'),
(31, 'n1', 'm1');

-- --------------------------------------------------------

--
-- 表的结构 `cp_yijian`
--

CREATE TABLE IF NOT EXISTS `cp_yijian` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `content` text NOT NULL,
  `dateline` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- 转存表中的数据 `cp_yijian`
--

INSERT INTO `cp_yijian` (`id`, `name`, `mobile`, `content`, `dateline`) VALUES
(1, '', 'fdgdsg', 'sdfs', 0),
(2, '', 'fdgdsg', 'sdfs', 1431308922),
(3, '', 'fdgdsg', 'sdfs', 1431308947),
(4, '', 'fdgdsg', 'sdfs', 1431308948),
(5, '', 'fdgdsg', 'sdfs', 1431308962),
(6, '', 'fdgdsg', 'sdfs', 1431310015),
(7, '', 'fdgdsg', 'sdfs', 1431310018),
(8, 'd', 'd', 'd', 1431502061),
(9, 'd', 'd', 'd', 1431502165),
(10, 'd', 'd', 'd', 1431502168),
(11, 'd', 'd', 'd', 1431502226),
(12, 'ds', 'd', 'd', 1431502370),
(13, 'x', 's', 'df', 1431505442),
(14, 'x', 's', 'dfsd', 1431505488),
(15, 'x', 's', 'dfsd', 1431505517),
(16, 'x', 's', 'sdfsd', 1431505522),
(17, 'xsd', 'ssd', 'sd', 1431505599),
(18, 'xsdff', 'ssd', 'dd', 1431505608),
(19, 'ds', 'd', 'd', 1431507412),
(20, 'ds', 'd', 'd', 1431507908),
(21, 'ds', 'd', 'd', 1431507917),
(22, 'df', 'sdf', 'sdf', 1431510122),
(23, 'df', 'sdf', 'sdf', 1431510266),
(24, 'df', 'sdf', 'sdf', 1431510516),
(25, 'df', 'sdf', 'sdf', 1431510569),
(26, 'df', 'sdf', 'sdf', 1431510600),
(27, 'df', 'sdf', 'sdf', 1431510602),
(28, 'df', 'sdf', 'sdf', 1431510638),
(29, 'df', 'sdf', 'sdf', 1431510640),
(30, 'df', 'sdf', 'sdf', 1431510642),
(31, 'df', 'sdf', 'sdf', 1431510686),
(32, 'sdf', 'sdf', 'sdf', 1431510770),
(33, 'sdf', 'sdf', 'sdf', 1431510777);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
